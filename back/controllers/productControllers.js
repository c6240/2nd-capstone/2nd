const Products = require('./../models/productModels');



module.exports.create = async (reqBody) => {
	const {productName, description, price} = reqBody
	
	let newProducts = new Products({
		productName:productName,
		description: description,
		price: price
	})

	return await newProducts.save().then((result, err) => result ? result : err)
}





module.exports.getAllProducts = async () => {

	return await Products.find().then(result => result)
}





module.exports.activeProducts = async () => {

	return await Products.find({isActive:true}).then(result => result)
}


module.exports.getAProduct = async (id) => {

	return await Products.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `Product not found`}
			}else{
				return err
			}
		}
	})
}




module.exports.updateProduct = async (productId, reqBody) => {

	return await Products.findByIdAndUpdate(productId, {$set: reqBody}, {new:true}).then(result => result)
}



module.exports.archive = async (productId) => {

	return await Products.findByIdAndUpdate(productId, {$set: {isActive: false}}, {new:true}).then(result => result)
}




module.exports.unArchive = async (productId) => {

	return await Products.findByIdAndUpdate(productId, {$set: {isActive: true}}, {new:true}).then(result => result)
}



module.exports.deleteProduct= async (productId) => {

	return await Products.findByIdAndDelete(productId).then((result, err) => result ? true : err)
}
