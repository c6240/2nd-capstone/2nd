const CryptoJS = require("crypto-js");
const User = require('./../models/userModels')
const Course = require('./../models/orderModels')
const {createToken} = require('./../auth');


//REGISTER A USER
module.exports.register = async (reqBody) => {
	const {firstName, lastName, email, password} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName, 
		email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
	})

	return await  newUser.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


//GET ALL USERS
module.exports.getAllUsers = async () => {

	return await User.find().then(result => result)
}


//CHECK IF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) => {
	const {email} = reqBody

	return await User.findOne({email: email}).then((result, err) =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			} else {
				return err
			}
		}
	})
}



//LOGIN A USER
module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){

				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
			

				if(reqBody.password == decryptedPw){
					return { token: createToken(result) }
				} else {
					return {auth: `Auth Failed!`}
				}

			} else {
				return err
			}
		}
	})
}


//CHANGE TO ADMIN STATUS TO TRUE
module.exports.adminStatus = async (reqBody) => {
	const {email} = reqBody

	return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
}



//CHANGE TO ADMIN STATUS TO FALSE
module.exports.userStatus = async (reqBody) => {

	return await User.findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin: false}}).then((result, err) => result ? true : err)
}




//DELETE A USER
module.exports.deleteUser = async (reqBody) => {
	const {email} = reqBody
	return await User.findOneAndDelete({email: email}).then((result, err) => result ? true : err)
}



// ORDER A PRODUCT
module.exports.order = async (data) => {
	const {userId, courseId} = data

	const updatedUser = await User.findById(userId).then(result => {
		result.transactions.push({courseId: courseId})

		return result.save().then(user => user ? true : false)
	})



	const updatedCourse = await Course.findById(courseId).then(result => {
		result.enrollees.push({userId: userId})

		return result.save().then(course => course ? true : false)
	})


	if(updatedUser && updatedCourse){
		return true

	} else {
		false
	}


}
