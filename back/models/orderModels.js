const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    quantity: {
        type: Number,
        require: [true, "Order number is required"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    userId: {
        type: String,
        require: [true, 'UserId is required']
    },
    transactions:[{
        productId: {
            type: String,
            require: [true, 'ProductId is required']
        }
    }]

}, {timestamps: true})

module.exports = mongoose.model("Orders", orderSchema);