const express = require('express');
const router = express.Router();
const {verifyAdmin} = require('./../auth.js')
const {register, getAllUsers,checkEmail,login,adminStatus,userStatus,deleteUser,enroll, order} = require('./../controllers/userControllers.js')

router.get('/', async (req, res) => {

    try {
        await getAllUsers().then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }


})

router.post(`/register`, async (req, res) => {
    try {
        await register(req.body).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }



})

router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})

router.post('/login', (req,res) => {
    try{    
        login(req.body).then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }
})

router.patch('/isAdmin', verifyAdmin, async (req,res) => {
    try{
           await adminStatus(req.body).then(result => res.send(result)) 
    } catch(err){
        res.status(500).json(err)
    }
})

router.patch('/isUser', verifyAdmin, async (req, res) => {
    try{
        await userStatus(req.body).then( result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})

router.delete('/delete-user', verifyAdmin, async (req, res) => {
    try{
        await deleteUser(req.body).then( result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})




module.exports = router;
