const express = require('express')
const router = express.Router()

const {
	create, 
	getAllProducts,
	getAProduct,
	updateProduct,
	Archive,
	unArchive,
	activeProducts,
	deleteProduct

} = require('./../controllers/productControllers.js')

const {verifyAdmin, verify} = require('../auth')


router.post('/create', verifyAdmin, async (req, res) => {
	try{
		create(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.get('/', verify, async (req, res) => {

	try{
		await getAllProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})




router.get('/isActive', verify, async (req, res) => {
	try{
		await activeProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.get('/:productId', verify, async (req, res) => {
	try{
		await getAProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




router.put('/:productId/update', verifyAdmin, async (req, res) => {
	try{
		await updateProduct(req.params.productId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})





router.patch('/:productId/archive', verifyAdmin, async (req, res) => {
	try{
		await Archive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



router.patch('/:productId/unarchive', verifyAdmin, async (req, res) => {
	try{
		await unArchive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.delete('/:productId/delete-Product', verifyAdmin, async (req, res) => {
	try{
		await deleteProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})





module.exports = router;