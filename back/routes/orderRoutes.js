const express = require('express');
const router = express.Router();
const {verify, decode} = require('./../auth.js')




router.post('/order', verify, async (req, res) => {
    const user = decode(req.headers.authorization).isAdmin
    const data = {
        userId: decode(req.headers.authorization).id,
        productId: req.body.productId 
    }

    if(user == false){
        try{
            await order(data).then(result => res.send(result))
        } catch (err){
            res.status(500).json(err)
        }
    } else{
        res.send(`Only with user access can order`)
    }

})